let alumno = [{
    "matricula":"2021030652",
    "nombre":"Quezada Lara Jesus Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/1.jpeg">'
},{
    "matricula":"2021030328",
    "nombre":"Garcia Gonzalez Jorge Enrique",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/2.jpeg">'
},{
    "matricula":"2021030652",
    "nombre":"Pastrano Navarro Brandon Rogelio",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/3.jpeg">'
},{ 
    "matricula":"2021030142",
    "nombre":"Aguilar Romero Jonathan Jesus",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/4.jpeg">'
},{
    "matricula":"2021030143",
    "nombre":"Tirado Rios Oscar",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/5.jpeg">'
},{
    "matricula":"2019030880",
    "nombre":"Quezada Ramos Julio Emiliano",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/6.jpeg">'
},{
    "matricula":"2021030143",
    "nombre":"Tirado Rios Oscar",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/7.jpeg">'
},{
    "matricula":"2021030311",
    "nombre":"Reyes Lizarraga Jonathan Alexis",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/8.jpeg">'
},{
    "matricula":"2021030314",
    "nombre":"Peñaloza Pizarro Felipe Andres",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/9.jpeg">'
},{
    "matricula":"2020030321",
    "nombre":"Ontiveros Govea Yair Alejandro",
    "grupo":"TI-73",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/10.jpeg">'
}
];
let tabla = document.getElementById('tabla-alumnos');



for (i=0;i<alumno.length;i++){
    let row = tabla.insertRow(); // Inserta una fila en la tabla
    // Inserta celdas en la fila
let cell1 = row.insertCell(0); // Matricula
let cell2 = row.insertCell(1); // Nombre
let cell3 = row.insertCell(2); // Grupo
let cell4 = row.insertCell(3); // Carrera
let cell5 = row.insertCell(4); // Foto

// Agrega los datos del alumno a las celdas
cell1.innerHTML = alumno[i].matricula;
cell2.innerHTML = alumno[i].nombre;
cell3.innerHTML = alumno[i].grupo;
cell4.innerHTML = alumno[i].carrera;
cell5.innerHTML = alumno[i].foto;
}

// Itera sobre el arreglo de alumnos y agrega filas a la tabla


/*console.log("Matricula:"+alumno.matricula);
console.log("Nombre:"+alumno.nombre);
console.log("Grupo:"+alumno.grupo);
console.log("Carrera:"+alumno.carrera);
console.log("Foto:"+alumno.foto);

alumno.nombre="Acosta Diaz Maria";
console.log("Nuevo nombre:"+alumno.nombre);

//Objetos compuestos

let cuentaBanco={
    "numero":"10001",
    "banco":"banorte",
    cliente:{
        "nombre":"Jose lopez",
        "fechaNac":"2020-01-01",
        "sexo":"M"},
        "saldo":"1000",
}
console.log("Nombre:"+cuentaBanco.cliente.nombre);
console.log("Saldo:"+cuentaBanco.saldo);
cuentaBanco.cliente.sexo="F";
console.log(cuentaBanco.cliente.sexo);

//arreglo productos
let productos=[{
    "codigo":"1001",
    "descripcion":"Atun",
    "precio":"34"},
    {
    "codigo":"1002",
    "descripcion":"Jabon en polvo",
    "precio":"23"},
    {
    "codigo":"1003",
    "descripcion":"Harina",
    "precio":"43"},
        {
    "codigo":"1004",
    "descripcion":"Pasta dental",
    "precio":"78"
}]

//mostrar un atributo de un objeto del arreglo
console.log("La descripcion es:"+productos[0].descripcion);
for(let i=0;i<productos.length;i++){
    console.log("Codigo: "+productos[i].codigo);
    console.log("Descripcion: "+productos[i].descripcion);
    console.log("Precio: "+productos[i].precio);
}*/